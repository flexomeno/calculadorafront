import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Container from "react-bootstrap/Container";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

class MyForm extends React.Component {
  constructor(props) {
    super(props);
//    this.state = { username: '' };
    this.state = { resultados: 0 };
  }
    

	funcion = (event) => {
	this.setState({funcion: event.target.value});
//    this.setState({username: this.state.number1});
	    	axios.get('http://calc-ws.herokuapp.com/' + this.state.funcion + '/' + this.state.numero1)
		.then(response => this.setState({resultados: response.data.result}))

  } 
  
  myChangeHandler = (event) => {
    this.setState({numero1: event.target.value});
//    this.setState({username: this.state.number1}); 
 

	} 
  render() {  
    return (
	
	<Container>

	
	<div class='calculator card'>
<center><h1><b>Parametro:</b></h1></center>
<input type='text' class='calculator-input z-depth-1' id='parametro' onChange={this.myChangeHandler} value={this.state.numero1}/>

<center><h1><b>Consulta:</b></h1></center>
<input type='text' class='calculator-screen z-depth-1' value={'http://calc-ws.herokuapp.com/' + this.state.funcion + '/' + this.state.numero1} disabled />

<center><h1><b>Función:</b></h1></center>
<input type='text' class='calculator-screen z-depth-1' value={this.state.funcion} disabled />

<center><h1><b>Resultado:</b></h1></center>
<input type='text' class='calculator-screen z-depth-1' value={this.state.resultados} disabled />


<div class='calculator-keys'>

  <button type='button' class='operator btn btn-info' value='tempf' onClick={this.funcion}>a C°</button>
  <button type='button' class='operator btn btn-info' value='tempc' onClick={this.funcion}>a F°</button>
  <button type='button' class='operator btn btn-info' value='seno' onClick={this.funcion}>Seno</button>
  <button type='button' class='operator btn btn-info' value='cop_usd' onClick={this.funcion}>USD</button>

  <button type='button' class='operator btn btn-info' value='usd_cop' onClick={this.funcion}>COP</button>
  <button type='button' class='operator btn btn-info' value='-'>+</button>
  <button type='button' class='operator btn btn-info' value='*'>-</button>
  <button type='button' class='operator btn btn-info' value='/'>*</button>

</div>
</div>
	
	</Container>
     
    );
  }
}

ReactDOM.render(<MyForm />, document.getElementById('root'));

